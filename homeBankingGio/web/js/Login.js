class Login {
    //ctrl+f: para busqueda 
    static entrarLocal() {
        let userInput = document.querySelector("#user").value;// ingreso x teclado
        let passInput = document.querySelector("#pass").value;// ingreso x teclado
        let datoCuenta = BaseDeDatos.datos();// me llega la BBDD y lo guardo en una variable

        //muestra por consola el listado completo de usuarios
        datoCuenta.map(usuarioPorUsuario =>
            console.log(usuarioPorUsuario.nombre)
        );// recorree .map recorre el array

        //encuentra un usuario que se busque comparando uno a uno en la BBDD recorriendo el array(.find)
        let userBuscados = datoCuenta.find(usuarioPorUsuario =>
            usuarioPorUsuario.user === userInput);

        if (userBuscados === undefined) {
            // undefined lo utilozo en caso de no encotrar nada.
            //No encontro el usuario
            //Muestra mensaje de error
            document.querySelector("#mensajePnl").innerHTML = "Usuario o contraseña incorrecto.";
        } else {
            //Encontro el usuario
            //Debemos comparar el password
            if (userBuscados.pass === passInput) {
                //Muestra el panel de cuenta privada.
                Login.showPnlCuenta();
                //Oculta el panel de login publico.
                Login.hidePnlLogin();
                localStorage.setItem("miCuenta", JSON.stringify(userBuscados));
                Cuenta.consultarTpl();

            } else {
                //Muestra mensaje de error
                document.querySelector("#mensajePnl").innerHTML = "Usuario o contraseña incorrecto.";
            }
        }
        console.log("userBUscado:" + JSON.stringify(userBuscados));
    }

    static salirLocal() {
        //Oculto el panel de la cuenta privada
        Login.hidePnlCuenta();
        //Muestro el login publico.
        Login.showPnlLogin();
    }
    
    static entrarServer(){
        let userInput = document.querySelector("#user").value;
        let passInput = document.querySelector("#pass").value;
        Http.doGet("LoginServlet" + "?&user=" + userInput + "&pass=" + passInput);
    }

    static showPnlCuenta() {
        document.querySelector("#cuentaPnl").style.display = "flex";
    }
    static hidePnlLogin() {
        document.querySelector("#loginPnl").style.display = "none";

    }
    
    //muesta login y oculta cuenta 
    static showPnlLogin() {
        document.querySelector("#loginPnl").style.display = "block"; // bloquear block
    }
    static hidePnlCuenta() {
        document.querySelector("#cuentaPnl").style.display = "none";// ninguna none
    }
    
    // inicio 
    static init() {
        //Muestra panel de login publico
            Login.showPnlLogin();
        //Oculta panel de cuenta privada
            Login.hidePnlCuenta();
            
        //A los botones de Login y Logout le asigna metodos de la Clase, booton del menu.
        document.querySelector("#loginBtnLocal").setAttribute("onclick", "Login.entrarLocal();");
        document.querySelector("#loginBtnServer").setAttribute("onclick", "Login.entrarServer();");
        // cuenta..
        /* 1 */ document.querySelector("#extraerOpt").setAttribute("onclick", "Cuenta.extraerTpl();");
        /* 2 */ document.querySelector("#extraerBtn").setAttribute("onclick", "Cuenta.extraerCalculo();");
        /* 3 */ document.querySelector("#consultarOpt").setAttribute("onclick", "Cuenta.consultarTpl();");
        /* 4 */ document.querySelector("#depositarOpt").setAttribute("onclick", "Cuenta.depositarTpl()");
        /* 5 */ document.querySelector("#depositarBtn").setAttribute("onclick", "Cuenta.depositarCalculo();");
        /* 6 */ document.querySelector("#limiteOpt").setAttribute("onclick", "Cuenta.limiteTpl()");
        /* 7 */ document.querySelector("#limiteBtn").setAttribute("onclick", "Cuenta.limiteCalculo();");
        /* 8 */ document.querySelector("#movimientosOpt").setAttribute("onclick", "Cuenta.movimientoTpl();");
        
        // saida de booton 
        document.querySelector("#logoutBtn").setAttribute("onclick", "Login.salirLocal();");
    }
}